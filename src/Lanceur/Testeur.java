package Lanceur;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Scanner;

import Classe.ConnectionSQL;
import Classe.Period;
import Classe.Revue;
import Classe.Abonnement;
import Classe.Client;

public class Testeur {

	private static Scanner clavier;


	public static void main(String[] args) throws SQLException, IOException {

		Connection maConnexion = ConnectionSQL.creerConnexion("jdbc:mysql://infodb.iutmetz.univ-lorraine.fr:3306/fassio2u_cpoa","fassio2u_appli","31606573");
		Period Per = new Period(maConnexion);
		Revue Rev = new Revue(maConnexion);
		Client Cli = new Client(maConnexion);
		Abonnement Abo = new Abonnement(maConnexion);

		Scanner clavier = new Scanner(System.in);

		int choix;
		do {
			System.out.println("Quel table voulez-vous modifier ?");
			System.out.println("1-PERIODICITE");
			System.out.println("2-REVUE");
			System.out.println("3-CLIENT");
			System.out.println("4-ABONNEMENT");
			System.out.println("");
			System.out.println("0-Quitter");

			choix = clavier.nextInt();
			switch (choix) {
			case 1:
				gerePeriodicite(Per);
				break;	
			case 2:
				gereRevue(Rev);
				break;
			
			case 3:
				gereClient(Cli);
				break;
			case 4:
				gereAbonnement(Abo);
				break;	
			
			}
		} while (choix > 0);

		clavier.close();

		if (maConnexion != null) {
			maConnexion.close();
		}
	}


	private static void gerePeriodicite(Period requetes) {

		System.out.println("Vous avez choisi : Périodicité");

		int choix;
		do {
			System.out.println("Quelle action choisissez-vous ?");
			System.out.println("1-Ajouter une période");
			System.out.println("2-Modifier une période");
			System.out.println("3-Supprimer une période");
			System.out.println("");
			System.out.println("0-Revenir au menu principal");

			clavier = new Scanner(System.in);
			choix = clavier.nextInt();
			
			switch (choix) {
			case 1:
				System.out.println("Vous voulez ajouter une période.");
				System.out.println("Nom de la période à  ajouter ? ");
				String libelle = clavier.next();


				int idGenere = requetes.ajouterPeriodicite(libelle);
				System.out.println("Cette nouvelle période a l'identifiant " + idGenere);
				break;

			case 2:
				System.out.println("Vous voulez modifier une période.");
				System.out.println("Numéro de la période à  modifier ? ");
				int id = clavier.nextInt();

				String laPeriodicite = requetes.chercherPeriodicite(id);
				if (laPeriodicite != null) {
					System.out.println("Période à  modifier : " + laPeriodicite);

					System.out.println("Nouveau nom de la période ? ");
					libelle = clavier.next();
					requetes.modifierPeriodicite(id, libelle);
				}
				break;

			case 3:
				System.out.println("Vous voulez supprimer une période.");
				System.out.println("Numéro de la période à supprimer ? ");
				id = clavier.nextInt();

				laPeriodicite = requetes.chercherPeriodicite(id);
				if (laPeriodicite != null) {
					requetes.supprimerPeriodicite(id);
					System.out.println("Cette période a été supprimé.");
				}
				break;

			}
		} while (choix > 0);
	}
	
	private static void gereRevue(Revue requetes) {

		System.out.println("Vous avez choisi : Revue");

		int choix;
		do {
			System.out.println("Quelle action choisissez-vous ?");
			System.out.println("1-Ajouter une revue");
			System.out.println("2-Modifier une revue");
			System.out.println("3-Supprimer une revue");
			System.out.println("");
			System.out.println("0-Revenir au menu principal");

			clavier = new Scanner(System.in);
			choix = clavier.nextInt();

			switch (choix) {

			case 1:
				System.out.println("Vous voulez ajouter une revue.");
				System.out.println("Titre de la revue ? ");
				String titre = clavier.next();
				System.out.println("Description de la revue ? ");
				String description = clavier.next();
				System.out.println("Tarif de la revue ? ");
				float tarif = clavier.nextFloat();
				System.out.println("Visuel de la revue ? ");
				String visuel = clavier.next();
				System.out.println("Périodicité de la revue ? ");
				int id_periodicite = clavier.nextInt();

				int idGenere = requetes.ajouterRevue(titre, description, tarif, visuel, id_periodicite);
				System.out.println("Cette nouvelle revue a l'identifiant " + idGenere);
				break;

			case 2:
				System.out.println("Vous voulez modifier une revue.");
				System.out.println("Num�ro de la revue à modifier ? "); 
				int id = clavier.nextInt();
				String leType = requetes.chercherRevue(id);
				if (leType != null) {
					System.out.println("Revue � modifier : " + leType);

					System.out.println("Nouveau titre de cette revue ? ");
					titre = clavier.next();
					System.out.println("Nouvelle description de cette revue ? ");
					description = clavier.next();
					System.out.println("Nouveau tarif de cette revue ? ");
					tarif = clavier.nextFloat();
					System.out.println("Nouveau visuel de cette revue ? ");
					visuel = clavier.next();
					System.out.println("Nouvelle p�riodicit� de cette revue ? ");
					id_periodicite = clavier.nextInt();

					requetes.modifierRevue(id, titre , description , tarif, visuel, id_periodicite);
				}
				break;

			case 3:
				System.out.println("Vous voulez supprimer une revue.");
				System.out.println("Num�ro de la revue � supprimer ? ");
				id = clavier.nextInt();
				leType = requetes.chercherRevue(id);
				if (leType != null) {
					requetes.supprimerRevue(id);
					System.out.println("Cette revue a �t� supprim�e.");
				}
				break;
			}
		} while (choix > 0);
	}

private static void gereClient(Client requetes) {

	System.out.println("Vous avez choisi : Client");

	int choix;
	do {
		System.out.println("Quel client choisissez-vous ?");
		System.out.println("1-Ajouter un client");
		System.out.println("2-Modifier un client");
		System.out.println("3-Supprimer un client");
		System.out.println("");
		System.out.println("0-Revenir au menu principal");

		Scanner clavier = new Scanner(System.in);
		choix = clavier.nextInt();

		switch (choix) {

		case 1:
			System.out.println("Vous voulez ajouter un client.");
			System.out.println("Nom du client ? ");
			String nom = clavier.next();
			System.out.println("Prenom du client ? ");
			String prenom = clavier.next();
			System.out.println("Num�ro de rue du client ? ");
			String no_rue = clavier.next();
			System.out.println("Voie du client ? ");
			String voie = clavier.next();
			System.out.println("Code postal du client ? ");
			String code_postal = clavier.next();
			System.out.println("Ville du client ? ");
			String ville = clavier.next();
			System.out.println("Pays du client ? ");
			String pays = clavier.next();


			int idGenere = requetes.ajouterClient(nom, prenom, no_rue, voie, code_postal, ville, pays);
			System.out.println("Ce nouveau client a l'identifiant " + idGenere);
			break;

		case 2:
			System.out.println("Vous voulez modifier un client.");
			System.out.println("Num�ro du client � modifier ? ");
			int id = clavier.nextInt();
			String leType = requetes.chercherClient(id);
			if (leType != null) {
				System.out.println("Client � modifier : " + leType);

				System.out.println("Nouveau nom de ce client ? ");
				nom = clavier.next();
				System.out.println("Nouveau prenom de ce client ? ");
				prenom = clavier.next();				
				System.out.println("Nouveau num�ro de rue de ce client ? ");
				no_rue = clavier.next();				
				System.out.println("Nouveau voie de ce client ? ");
				voie = clavier.next();				
				System.out.println("Nouveau code postal de ce client ? ");
				code_postal = clavier.next();				
				System.out.println("Nouvelle ville de ce client ? ");
				ville = clavier.next();
				System.out.println("Nouveau pays de ce client ? ");
				pays = clavier.next();
				
				requetes.modifierClient(id,nom, prenom, no_rue, voie, code_postal, ville, pays);
			}
			break;

		case 3:
			System.out.println("Vous voulez supprimer un client.");
			System.out.println("Num�ro du client � supprimer ? ");
			id = clavier.nextInt();
			leType = requetes.chercherClient(id);
			if (leType != null) {
				requetes.supprimerClient(id);
				System.out.println("Ce client a �t� supprim�e.");
			}
			break;
		}
	} while (choix > 0);
}


private static void gereAbonnement(Abonnement requetes) {

	System.out.println("Vous avez choisi : Abonnement");

	int choix;
	do {
		System.out.println("Quel client choisissez-vous ?");
		System.out.println("1-Ajouter un abonnement");
		System.out.println("2-Modifier un abonnement");
		System.out.println("3-Supprimer un abonnement");
		System.out.println("");
		System.out.println("0-Revenir au menu principal");

		Scanner clavier = new Scanner(System.in);
		choix = clavier.nextInt();

		switch (choix) {

		case 1:
			System.out.println("Vous voulez ajouter un abonnement.");
			System.out.println("Id du client ? ");
			int id = clavier.nextInt();
			System.out.println("Id de la revue ? ");
			int id_revue = clavier.nextInt();
			System.out.println("Date du d�but (yyyy-mm-dd) ? ");
			String date_debut = clavier.next();
			System.out.println("Date de fin (yyyy-mm-dd) ? ");
			String date_fin = clavier.next();


			int idGenere = requetes.ajouterAbonnement(id, id_revue, date_debut, date_fin);
			System.out.println("Ce nouveau abonnement a l'identifiant " + idGenere);
			break;

		case 2:
			System.out.println("Vous voulez modifier un abonnement.");
			System.out.println("Num�ro du client de l'abonnement � modifier ? ");
			int id_client = clavier.nextInt();
			
			String leType = requetes.chercherAbonnement(id_client);
			if (leType != null) {
				System.out.println("Abonnement � modifier : " + leType);
				System.out.println("Nouveau num�ro de revu ? ");
				id_revue = clavier.nextInt();
				System.out.println("Nouvelle date de debut ? ");
				date_debut = clavier.next();
				System.out.println("Nouvelle date de fin ? ");
				date_fin = clavier.next();	
				
				requetes.modifierAbonnement(id_client, id_revue, date_debut, date_fin);
			}
			break;

		case 3:
			System.out.println("Vous voulez supprimer un abonnement.");
			System.out.println("Num�ro du client � supprimer ? ");
			id= clavier.nextInt();
			leType = requetes.chercherAbonnement(id);
			if (leType != null) {
				requetes.supprimerAbonnement(id);
				System.out.println("Cet abonnement a �t� revue supprim�e .");
			}
			break;
		}
	} while (choix > 0);
}
}