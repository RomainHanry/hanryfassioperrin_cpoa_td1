package Classe;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class Abonnement {

	private Connection connexion;

	public Abonnement(Connection co) {

		this.connexion = co;
	}

	public String chercherAbonnement(int id_client) {

		String revue=null;
		try {
			PreparedStatement requete = connexion.prepareStatement("SELECT * FROM Abonnement WHERE id_client=? ");
			requete.setInt(1, id_client);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				revue = 
						"id client = " + res.getInt("id_client") +
						"id revue = " + res.getInt("id_revue") +
						" date debut = " + res.getTime("date_debut") +
						" date fin = " + res.getTime("date_fin");
				res.close();
			} else {
				System.out.println("Abonnement inexistante !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return revue;
	}

	public int ajouterAbonnement(int id_client, int id_revue, String date_debut, String date_fin) {


		try {
			PreparedStatement requete = connexion.prepareStatement(
					"INSERT INTO Abonnement (id_client, id_revue, date_debut, date_fin) VALUES (?,?,?,?)");
			requete.setInt(1, id_client);
			requete.setInt(2, id_revue);
			requete.setString(3, date_debut);
			requete.setString(4, date_fin);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

		return id_client ;
	}
	
	public void modifierAbonnement(int id_client, int id_revue, String date_debut, String date_fin) {

		try {
			PreparedStatement requete = connexion.prepareStatement(
					"UPDATE Abonnement SET id_revue=?, date_debut=?, date_fin=? WHERE id_client=? ");
			requete.setInt(1, id_revue);
			requete.setString(2, date_debut);
			requete.setString(3, date_fin);
			requete.setInt(4, id_client);
		
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

	}

	public void supprimerAbonnement(int id_client) {

		try {
			PreparedStatement requete = connexion.prepareStatement("DELETE FROM Abonnement WHERE id_client=? ");
			requete.setInt(1, id_client);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}


}

