package Classe;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionSQL {

	
	public static Connection creerConnexion(String url, String login, String password) {

		Connection maConnexion = null;

		try {
			maConnexion = DriverManager.getConnection(url, login, password);
		} catch (SQLException se) {
			System.out.println("Erreur de connexion :" + se.getMessage());
		}

		return maConnexion;
	}

}
