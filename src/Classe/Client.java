package Classe;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Client {

	private Connection connexion;

	public Client(Connection co) {

		this.connexion = co;
	}

	public String chercherClient(int id) {

		String revue=null;
		try {
			PreparedStatement requete = connexion.prepareStatement("SELECT * FROM Client WHERE id_client=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				revue = 
						"id = " + res.getInt("id_client") +
						" nom = " + res.getString("nom") +
						" prenom = " + res.getString("prenom") +
						" no_rue = " + res.getString("no_rue") +
						" voie = " + res.getString("voie") +
						" code_postal = " + res.getString("code_postal") +
						" ville = " + res.getString("ville") +
						" pays = " + res.getString("pays") ;
				res.close();
			} else {
				System.out.println("Client inexistant !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return revue;
	}

	public int ajouterClient(String nom, String prenom, String no_rue, String voie, String code_postal, String ville, String pays) {

		int id = -1;

		try {
			PreparedStatement requete = connexion.prepareStatement(
					"INSERT INTO Client (nom, prenom, no_rue, voie, code_postal, ville, pays) VALUES (?,?,?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setString(1, nom);
			requete.setString(2, prenom);
			requete.setString(3, no_rue);
			requete.setString(4, voie);
			requete.setString(5, code_postal);
			requete.setString(6, ville);
			requete.setString(7, pays);
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				id = res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

		return id;
	}
	
	public void modifierClient(int id, String nom, String prenom, String no_rue, String voie, String code_postal, String ville, String pays ) {

		try {
			PreparedStatement requete = connexion.prepareStatement(
					"UPDATE Client SET nom=?, prenom=?, no_rue=?, voie=?, code_postal=?, ville=?, pays=? WHERE id_client=?");
			requete.setString(1, nom);
			requete.setString(2, prenom);
			requete.setString(3, no_rue);
			requete.setString(4, voie);
			requete.setString(5, code_postal);
			requete.setString(6, ville);
			requete.setString(7, pays);
			requete.setInt(7, id);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

	}

	public void supprimerClient(int id) {

		try {
			PreparedStatement requete = connexion.prepareStatement("DELETE FROM Client WHERE id_client=?");
			requete.setInt(1, id);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}
}