package Classe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Period {

	private Connection connexion;

	public Period(Connection co) {

		this.connexion = co;
	}

	public String chercherPeriodicite(int id) {

		String periodicite=null;
		try {
			PreparedStatement requete = connexion.prepareStatement("SELECT * FROM Periodicite WHERE id_periodicite=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				periodicite = 
						"id = " + res.getInt("id_periodicite") +
						" libellé = " + res.getString("libelle") ;
				res.close();
			} else {
				System.out.println("Période inexistante !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return periodicite;
	}

	public int ajouterPeriodicite(String libelle) {

		int id = -1;

		try {
			PreparedStatement requete = connexion.prepareStatement(
					"INSERT INTO Periodicite (libelle) VALUES (?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setString(1, libelle);
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				id = res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

		return id;
	}

	public void modifierPeriodicite(int id, String libelle) {

		try {
			PreparedStatement requete = connexion.prepareStatement(
					"UPDATE Periodicite SET libelle=? WHERE id_periodicite=?");
			requete.setString(1, libelle);
			requete.setInt(2, id);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

	}

	public void supprimerPeriodicite(int id) {

		try {
			PreparedStatement requete = connexion.prepareStatement("DELETE FROM Periodicite WHERE id_periodicite=?");
			requete.setInt(1, id);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}
}
