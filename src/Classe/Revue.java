package Classe;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Revue {

	private Connection connexion;

	public Revue(Connection co) {

		this.connexion = co;
	}

	public String chercherRevue(int id) {

		String revue=null;
		try {
			PreparedStatement requete = connexion.prepareStatement("SELECT * FROM Revue WHERE id_revue=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			if (res.next()) {
				revue = 
						"id = " + res.getInt("id_revue") +
						" titre = " + res.getString("titre") +
						" description = " + res.getString("description") +
						" tarif = " + res.getFloat("tarif_numero") +
						" visuel = " + res.getString("visuel") +
						" id période = " + res.getInt("id_periodicite") ;
				res.close();
			} else {
				System.out.println("Revue inexistante !");
			}

			if (requete != null) {
				requete.close();
			}

		} catch (SQLException se) {
			System.out.println("Pb SQL " + se.getMessage());
		}

		return revue;
	}

	public int ajouterRevue(String titre, String description, float tarif, String visuel, int id_periodicite) {

		int id = -1;

		try {
			PreparedStatement requete = connexion.prepareStatement(
					"INSERT INTO Revue (titre, description, tarif_numero, visuel, id_periodicite) VALUES (?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			requete.setString(1, titre);
			requete.setString(2, description);
			requete.setFloat(3, tarif);
			requete.setString(4, visuel);
			requete.setInt(5, id_periodicite);
			requete.executeUpdate();

			ResultSet res = requete.getGeneratedKeys();
			if (res.next()) {
				id = res.getInt(1);
				res.close();
			}

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

		return id;
	}
	
	public void modifierRevue(int id, String titre, String description, float tarif, String visuel, int id_periodicite) {

		try {
			PreparedStatement requete = connexion.prepareStatement(
					"UPDATE Revue SET titre=?, description=?, tarif_numero=?, visuel=?, id_periodicite=? WHERE id_revue=?");
			requete.setString(1, titre);
			requete.setString(2, description);
			requete.setFloat(3, tarif);
			requete.setString(4, visuel);
			requete.setInt(5, id_periodicite);
			requete.setInt(6, id);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}

	}

	public void supprimerRevue(int id) {

		try {
			PreparedStatement requete = connexion.prepareStatement("DELETE FROM Revue WHERE id_revue=?");
			requete.setInt(1, id);
			requete.executeUpdate();

			if (requete != null) {
				requete.close();
			}
		} catch (SQLException se) {
			System.err.println("Pb SQL :" + se.getMessage());
		}
	}
}

